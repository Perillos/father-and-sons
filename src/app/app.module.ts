import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FatherComponent } from './components/father/father.component';
import { Son2Component } from './components/father/son2/son2.component';
import { Son1Component } from './components/father/son1/son1.component';

@NgModule({
  declarations: [
    AppComponent,
    FatherComponent,
    Son2Component,
    Son1Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
