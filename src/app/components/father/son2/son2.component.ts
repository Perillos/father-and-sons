import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-son2',
  templateUrl: './son2.component.html',
  styleUrls: ['./son2.component.scss']
})
export class Son2Component implements OnInit {
  @Input() public inputText!: string

  constructor() { }

  ngOnInit(): void {
  }

}
