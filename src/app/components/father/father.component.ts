import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-father',
  templateUrl: './father.component.html',
  styleUrls: ['./father.component.scss']
})
export class FatherComponent implements OnInit {
  public inputText: string = '';
  public sonMessage: string = '';

  public changeInputText(newText: string): void {
    this.inputText = newText;
  }

  public setSonMessage(message:string){
    this.sonMessage = message;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
