import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-son1',
  templateUrl: './son1.component.html',
  styleUrls: ['./son1.component.scss']
})
export class Son1Component implements OnInit {
  @Output() public emitMessage = new EventEmitter

  public sonMessage: string = ''

  public changeInputText(newText: string): void {
    this.sonMessage = newText
    this.emitMessage.emit(this.sonMessage)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
